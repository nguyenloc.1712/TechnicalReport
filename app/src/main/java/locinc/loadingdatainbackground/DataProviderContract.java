package locinc.loadingdatainbackground;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Loc on 5/12/2016.
 */
public class DataProviderContract implements BaseColumns{
    // The URI scheme used for content URIs
    public static final String SCHEME = "content";

    // The provider's authority
    public static final String AUTHORITY = "locinc.loadingdatainbackground";

    /**
     * The DataProvider content URI
     */
    public static final Uri CONTENT_URI = Uri.parse(SCHEME + "://" + AUTHORITY);

    public static final String SONG_TABLE_NAME = "songs";

    public static final Uri SONG_TABLE_CONTENTURI =
            Uri.withAppendedPath(CONTENT_URI, SONG_TABLE_NAME);

    public static final String ROW_ID = BaseColumns._ID;

    public static final String SONG_TITLES_COLUMN = "title";
    public static final String SONG_SINGER_COLUMN = "singer";
    public static final String SONG_URL_COLUMN = "url";

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "data";
}
