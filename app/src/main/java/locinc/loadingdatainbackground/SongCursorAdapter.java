package locinc.loadingdatainbackground;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Created by Loc on 5/12/2016.
 */
public class SongCursorAdapter extends SimpleCursorAdapter{
    private Context mContext;
    private Context appContext;
    private int layout;
    private Cursor cr;
    private final LayoutInflater inflater;

    public SongCursorAdapter(Context context, int layout,
                             Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.layout=layout;
        this.mContext = context;
        this.inflater=LayoutInflater.from(context);
        this.cr=c;
    }

    @Override
    public View newView (Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(layout, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        TextView title=(TextView)view.findViewById(R.id.song_title);
        TextView artist=(TextView)view.findViewById(R.id.song_singer);

        int position=cursor.getColumnIndexOrThrow(DataProviderContract.ROW_ID);
        int titleIndex = cursor.getColumnIndexOrThrow(DataProviderContract.SONG_TITLES_COLUMN);
        int artistIndex = cursor.getColumnIndexOrThrow(DataProviderContract.SONG_SINGER_COLUMN);
        int urlIndex = cursor.getColumnIndexOrThrow(DataProviderContract.SONG_URL_COLUMN);

        title.setText(cursor.getString(titleIndex));
        artist.setText(cursor.getString(artistIndex));

    }
}