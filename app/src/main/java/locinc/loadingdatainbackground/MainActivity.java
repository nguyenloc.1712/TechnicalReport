package locinc.loadingdatainbackground;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor>{

    private ListView songListView;
    private DrawerLayout mDrawerLayout;
    private SongCursorAdapter notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.main_layout);
        songListView = (ListView) findViewById(R.id.list_song);

        getLoaderManager().initLoader(0, null, this);
        fillData();
    }

    private void fillData() {
        // Create an array to specify the fields we want to display in the list (only TITLE)
        String[] from = new String[]{DataProviderContract.SONG_TITLES_COLUMN};

        // and an array of the fields we want to bind those fields to (in this case just text1)
        int[] to = new int[]{R.id.song_title};

        // Now create a simple cursor adapter and set it to display
        notes = new SongCursorAdapter(this, R.layout.song_thumbnail, null, from, to, 0);
        songListView.setAdapter(notes);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//     Takes action based on the ID of the Loader that's being created
        String[] mProjection = {DataProviderContract.ROW_ID, DataProviderContract.SONG_TITLES_COLUMN,
                DataProviderContract.SONG_SINGER_COLUMN, DataProviderContract.SONG_URL_COLUMN};
        Log.v("URI", DataProviderContract.SONG_TABLE_CONTENTURI.toString());
        switch (id) {
            case 0:
                // Returns a new CursorLoader
                return new CursorLoader(this,   // Parent activity context
                        DataProviderContract.SONG_TABLE_CONTENTURI,        // Table to query
                        mProjection,     // Projection to return
                        null,            // No selection clause
                        null,            // No selection arguments
                        null             // Default sort order
                );
            default:
                // An invalid id was passed in
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        notes.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
