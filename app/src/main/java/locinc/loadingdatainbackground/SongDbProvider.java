package locinc.loadingdatainbackground;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;

/**
 * Created by Loc on 5/12/2016.
 */

public class SongDbProvider extends ContentProvider {

    private static final String TEXT_TYPE = "TEXT";
    private static final String PRIMARY_KEY_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";

    private static final String CREATE_TABLE_SONG = "CREATE TABLE" + " " +
            DataProviderContract.SONG_TABLE_NAME + " " +
            "(" + " " +
            DataProviderContract.ROW_ID + " " + PRIMARY_KEY_TYPE + " ," +
            DataProviderContract.SONG_TITLES_COLUMN + " " + TEXT_TYPE + " ," +
            DataProviderContract.SONG_SINGER_COLUMN + " " + TEXT_TYPE + " ," +
            DataProviderContract.SONG_URL_COLUMN + " " + TEXT_TYPE +
            ")";

    private SQLiteOpenHelper mHelper;
    public static final String LOG_TAG = "DataProvider";

    @Override
    public boolean onCreate() {
        // Creates a new database helper object
        mHelper = new DataProviderHelper(getContext());

        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor returnCursor = db.query(
                DataProviderContract.SONG_TABLE_NAME,
                projection,
                null, null, null, null, null);
        return returnCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        SQLiteDatabase localSQLiteDatabase = mHelper.getWritableDatabase();

        // Inserts the row into the table and returns the new row's _id value
        long id = localSQLiteDatabase.insert(
                DataProviderContract.SONG_TABLE_NAME,
                DataProviderContract.SONG_SINGER_COLUMN,
                values
        );

        // If the insert succeeded, notify a change and return the new row's content URI.
        if (-1 != id) {
            getContext().getContentResolver().notifyChange(uri, null);
            return Uri.withAppendedPath(uri, Long.toString(id));
        } else {
            throw new SQLiteException("Insert error:" + uri);
        }

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    public void close() {
        mHelper.close();
    }

    private class DataProviderHelper extends SQLiteOpenHelper {
        DataProviderHelper(Context context) {
            super(context,
                    DataProviderContract.DATABASE_NAME,
                    null,
                    DataProviderContract.DATABASE_VERSION);
        }

        private void dropTables(SQLiteDatabase db) {
            // If the table doesn't exist, don't throw an error
            db.execSQL("DROP TABLE IF EXISTS " + DataProviderContract.SONG_TABLE_NAME);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // Creates the tables in the backing database for this provider
            db.execSQL(CREATE_TABLE_SONG);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int version1, int version2) {
            Log.w(DataProviderHelper.class.getName(),
                    "Upgrading database from version " + version1 + " to "
                            + version2 + ", which will destroy all the existing data");

            // Drops all the existing tables in the database
            dropTables(db);

            // Invokes the onCreate callback to build new tables
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int version1, int version2) {
            Log.w(DataProviderHelper.class.getName(),
                    "Downgrading database from version " + version1 + " to "
                            + version2 + ", which will destroy all the existing data");

            // Drops all the existing tables in the database
            dropTables(db);

            // Invokes the onCreate callback to build new tables
            onCreate(db);

        }
    }
}